# Minecraft Colorize Blocks Generator

Minecraft用のPythonスクリプトです。  
テンプレートからModを自動生成します。  
`config.yaml`の設定に従い、様々な配色のブロックが追加されます。  
~~Java触りたくない人用。~~

- `config.yaml`から色名、カラーコードを読み込み
- `templates/`フォルダ以下のタグ(Mod名など)を置換、Javaファイル等にアウトプット
- ImageMagickを使用し、ブロックのテクスチャを自動生成
- その後ビルドを自動実行、出来上がった`modid-1.0.jar`を自動リネーム  
(mod名やバージョンが変わる度に`build.gradle`を編集する必要はありません。`config.yaml`を一度設定するだけで、その後の処理はスクリプトが全て自動実行します)

[Color Blocks 2](https://www.planetminecraft.com/mod/color-blocks/)のような動的にブロックを生成するModと比較すると自分でビルドする必要はありますが、シンプルな構造のためマルチプレイに対応しやすく、[BuildCraft](https://www.mod-buildcraft.com)の外装板等、他のModとも親和性があります。


## Usage

1. Set up the development environment of your mod (Gradle, Forge, etc…).
2. Edit `config.yaml`.
3. Create your templates in `templates/`.
4. Double click `build.command` (Mac) or `$ ./build.command`.
5. Copy `resources/package/yourmod.jar` to `mods/` or make symbolic link (for debug).

### Example
テンプレート部分はGPLv3ライセンスで別途公開しています。

- [Color Blocks Plus](https://gitlab.com/Isthmis/Color-Blocks-Plus)
- [ColorizeJIS](https://gitlab.com/Isthmis/ColorizeJIS)


## Require

- Bash
- Python 3.6.0
- PyYAML
- ImageMagick
- Gradle

Developed on macOS El Capitan (10.11.6).


## License

This project is licensed under the terms of the CC0-1.0 Universal license.

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg "CC0")](https://creativecommons.org/publicdomain/zero/1.0/)
